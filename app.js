// imports
const express = require('express');
const path = require('path');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dotenv = require('dotenv').config();

mongoose.Promise = global.Promise;

// env variables
const port = process.env.PORT;
const db_host = process.env.DB_HOST;
const db_name = process.env.DB_NAME;
const db_user = process.env.DB_USER;
const db_password = process.env.DB_PASS;

// mongoose models
const User = require('./models/User');
const Restaurant = require('./models/Restaurant');
const Reservation = require('./models/Reservation');
const Rating = require('./models/Rating');

// routers
const SignupRouter = require('./routes/SignupRouter')(User);
const LoginRouter = require('./routes/LoginRouter')(User);
const RestaurantRouter = require('./routes/RestaurantRouter')(Restaurant, Rating);
const ReservationRouter = require('./routes/ReservationRouter')(Reservation, Restaurant);

// helpers
const AuthConstructor = require('./helpers/auth');

// init
const app = express();
const auth = AuthConstructor(User);
app.use(auth.initialize());

// middlewares
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/signup', SignupRouter);
app.use('/login', LoginRouter);
app.use('/restaurants', auth.authenticate(), RestaurantRouter)
app.use('/reservation', auth.authenticate(), ReservationRouter)

app.get('/', auth.authenticate(), (req, res, next) => {
  res.json({ status: 'active' }).status(200);
});

app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.json({ errors: err }).status(err.status || 500);
});

const server = app.listen(port, () => {
  console.log(`Magic is happening on: ${server.address().address}/${port}.`);
  mongoose.connect(`mongodb://${db_user}:${db_password}@${db_host}/${db_name}`, err => {
    if (err) console.log('Connection Failed.')
    else console.log('Connected to Database.');
  });
});
