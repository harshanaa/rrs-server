const express = require('express');
const _ = require('lodash');
const { check, validationResult } = require('express-validator/check');
const jwt = require('jsonwebtoken');

const routerConstructor = (User) => {
  const LoginRouter = express.Router();

  LoginRouter.route('/')
    .post([
      check('username').isEmail().withMessage('Username should be a valid email').trim().normalizeEmail(),
      check('password').isLength({ min: 5 }).withMessage('Password should be atleast 5 char long')
    ], (req, res, next) => {
      const { username, password } = req.body;

      User.findOne({ username }, (err, user) => {
        if (err || _.isEmpty(user)) {
          return res.status(422).json({
            errors: [{
              "location": "body",
              "param": "Email",
              "value": username,
              "msg": "Could't find a user with that email"
            }]
          });
        }

        user.comparePassword(password, (err, isMatch) => {
          if (err) next(err)
          else if (isMatch) {
            const access_token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: "5h" });
            const response = {
              access_token,
              username: user.username,
              firstName: user.firstName,
              lastName: user.lastName
            }
            res.status(200).json(response)
          }
          else {
            return res.status(422).json({
              errors: [{
                "location": "body",
                "param": "Password",
                "value": password,
                "msg": "Credentials do not match"
              }]
            });
          }
        })
      })
    })

  return LoginRouter;
}

module.exports = routerConstructor;