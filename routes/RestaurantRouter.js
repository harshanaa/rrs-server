const express = require('express');
const _ = require('lodash');
const { check, validationResult } = require('express-validator/check');

const routerConstructor = (Restaurant, Rating) => {
  const RestaurantRouter = express.Router();

  RestaurantRouter.route('/')
    .get([
      check('near').isLength({ min: 6 }).optional().withMessage('Invalid Coordinates'),
      check('distance').isInt().optional().withMessage('Distance should be a int'),
      check('type').isString().optional().withMessage('type should be a string'),
      check('rating').isFloat().optional().withMessage('Rating should be a number')
    ], (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const {
        near = null,
        distance = 1,
        type = null,
        rating = null
      } = req.query;

      const filter = [
        { 'type': { $regex: new RegExp(type, "i") } },
      ];

      const query = Restaurant.find();

      if (near) {
        query.where('coordinates').near({
          center: near.trim().split(','),
          maxDistance: distance / 111.2
        });
      }

      if (type) {
        query.or(filter);
      }

      if (rating) {
        query.where('rating').equals(rating);
      }

      query
        .exec((error, result) => {
          if (error) res.status(500).json({ errors: error })
          else res.status(200).json(result);
        });
    })
    .post((req, res) => {
      // admin panel is not in the scope. This is just to add data
      const restaurant = new Restaurant(req.body);
      restaurant.save(err => {
        if (err) res.status(500).send(err)
        else res.status(201).json(restaurant)
      });
    });

  RestaurantRouter.route('/:restaurantId/rate')
    .post([
      check('restaurantId').isMongoId().withMessage("This restaurant id doesn't seem to be valid"),
      check('rating').isInt().withMessage("Invalid rating"),
    ], (req, res, next) => {
      const { restaurantId } = req.params;
      const { rating } = req.body;
      const { _id: userId } = req.user;

      Restaurant.findById(restaurantId, (err, restaurant) => {
        if (err || _.isEmpty(restaurant)) {
          return res.status(422).json({
            errors: [{
              "location": "body",
              "param": "restaurantId",
              "value": restaurantId,
              "msg": "Invalid restaurant id"
            }]
          });
        } else {
          const userRating = new Rating({
            restaurantId,
            userId,
            rating
          });

          userRating.save(err => {
            if (err) res.status(500).send(err)
            else res.status(201).json(userRating)
          });
        }
      })
    })

  return RestaurantRouter;
}

module.exports = routerConstructor;