const express = require('express');
var mongoose = require('mongoose');
const { ObjectId } = mongoose.Types;
const _ = require('lodash');
const { check, validationResult } = require('express-validator/check');
const moment = require('moment');
require('moment-timezone');
moment.tz.setDefault("Asia/Colombo");


const routerConstructor = (Reservation, Restaurant) => {
  const ReservationRouter = express.Router();

  ReservationRouter.route('/mine')
    .get([

    ], (req, res, next) => {

      Reservation.aggregate([
        {
          $match: {
            userId: req.user._id
          }
        },
        {
          $lookup: {
            from: 'restaurants',
            localField: "restaurantId",
            foreignField: "_id",
            as: "abc"
          }
        },
        {
          $group: {
            _id: '$date',
            data: {
              $push: {
                _id: '$_id',
                seats: '$seats',
                date: '$date',
                slot: '$slot',
                message: '$message',
                restaurantId: '$restaurantId',
                restaurant: '$abc'
              }
            }
          }
        }
      ])
        .exec((error, result) => {
          if (error) res.status(500).json({ errors: error })
          else res.status(200).json(result);
        })
    });

  ReservationRouter.route('/:reservationId')
    .get([
      check('expand').isBoolean().withMessage("Expand is not valid").optional(),
      check('reservationId').isMongoId().withMessage("This reservation id doesn't seem to be valid"),
    ], (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const { reservationId } = req.params;
      const { expand } = req.query;

      const query = Reservation.findById(reservationId);
      if (expand) {
        query
          .populate('restaurantId')
          .populate({
            path: 'userId',
            select: '-password',
          })
      }

      query
        .exec((error, result) => {
          if (error) res.status(500).json({ errors: error })
          else res.status(200).json(result);
        });
    })
    .delete([
      check('reservationId').isMongoId().withMessage("This reservation id doesn't seem to be valid"),
    ], (req, res, next) => {

      const { reservationId } = req.params;
      Reservation.findById(reservationId, function (err, result) {
        if (err || _.isEmpty(result)) {
          res.status(422).json({
            errors: [{
              "location": "query",
              "param": "reservationId",
              "value": reservationId,
              "msg": "Invalid reservation id"
            }]
          });
        }
        else {
          const { date, slot } = result;
          const reservationTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), slot.getHours(), slot.getMinutes(), slot.getSeconds());
          if (moment().add(1, 'hours').isAfter(moment(reservationTime))) { // dont delete if reservation is in less than an hour
            res.status(422).json({
              errors: [{
                "value": reservationTime,
                "msg": "Cannot remove reservations that are in less than an hour"
              }]
            });
            return;
          }
          result.remove();
          res.status(200).json({ msg: 'Item deleted' });
        }
      });
    })
    .post([
      check('reservationId').isMongoId().withMessage("This reservation id doesn't seem to be valid"),
      check('restaurantId').not().exists().withMessage("Restaurent cannot be changed on the reservation"),
      check('seats').isInt().withMessage("Invalid number of seats"),
      check('date').isISO8601().withMessage("Invalid reservation date"),
      check('message').optional().isString().withMessage("Invalid message"),
      check('date').exists().custom((value, { req }) => {
        // check if the date is future date
        const { date: reqDate, slot: reqSlot } = req.body;
        const date = new Date(reqDate);
        const slot = new Date(reqSlot);
        const reservation = new Date(date.getFullYear(), date.getMonth(), date.getDate(), slot.getHours(), slot.getMinutes(), slot.getSeconds());
        return new Date(reservation) > new Date()
      }).withMessage("Only future reservations can be edited"),
      check('slot').isISO8601().withMessage("Invalid reservation slot"),
    ], (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const { reservationId } = req.params;
      Reservation.findById(reservationId, function (err, reservation) {
        if (err || _.isEmpty(reservation)) {
          res.status(422).json({
            errors: [{
              "location": "query",
              "param": "reservationId",
              "value": reservationId,
              "msg": "Invalid reservation id"
            }]
          });
        } else {

          const { date, slot, seats, message, restaurantId } = reservation;
          const { date: reqDate, slot: reqSlot, seats: _seats, message: _message } = req.body;

          let _date = moment(reqDate).format('YYYY-MM-DD') + 'T00:00:00.000Z'; // we just need the date
          _date = new Date(_date);
          const _slot = moment(reqSlot).set({ year: 2001, month: 0, date: 1 }); // we need just the time

          const oldReservationTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), slot.getHours(), slot.getMinutes(), slot.getSeconds());
          const newReservationTime = new Date(_date.getFullYear(), _date.getMonth(), _date.getDate(), _slot.toDate().getHours(), _slot.toDate().getMinutes(), _slot.toDate().getSeconds());

          if (moment().add(1, 'hours').isAfter(moment(oldReservationTime))) { // dont delete if reservation is in less than an hour
            res.status(422).json({
              errors: [{
                "value": oldReservationTime,
                "msg": "Cannot edit reservations that are in less than an hour"
              }]
            });
            return;
          }

          Restaurant.findById(restaurantId, (err, restaurant) => {
            if (err || _.isEmpty(restaurant)) {
              res.status(500).json({
                errors: [{
                  "msg": "Internal error"
                }]
              });
            } else {

              const { serviceTimes, seats: seatCount } = restaurant;
              if (!serviceTimes.some(e => e.toISOString() === _slot.toISOString())) {
                res.status(422).json({
                  errors: [{
                    "location": "body",
                    "param": "slot",
                    "value": slot,
                    "msg": `Restaurent doesn't offer a ${_slot.format("h:mm a")} service`
                  }]
                });
                return;
              }

              Reservation.aggregate([
                {
                  $match: {
                    $and: [
                      { restaurantId: ObjectId(restaurantId) },
                      { date: _date },
                      { slot: _slot.toDate() },
                    ]
                  }
                },
                {
                  $group: { _id: null, total: { $sum: "$seats" } }
                }
              ], function (err, result) {
                if (err) {
                  res.status(422).json({
                    errors: [{
                      "location": "body",
                      "param": "restaurantId",
                      "value": restaurantId,
                      "msg": "Invalid restaurant id"
                    }]
                  });
                } else {

                  const isNewReservationOnSameDateAndTime = moment(oldReservationTime).isSame(newReservationTime);
                  const currentReservationCount = result[0] ? result[0].total : 0;

                  if (_.isEmpty(result) || isNewReservationOnSameDateAndTime && (currentReservationCount - seats) + _seats <= seatCount || !isNewReservationOnSameDateAndTime && (currentReservationCount + _seats <= seatCount)) {

                    reservation.seats = _seats ? _seats : seats;
                    reservation.message = _message ? _message : message;
                    reservation.date = _date ? _date : date;
                    reservation.slot = _slot ? _slot : slot;

                    reservation.save(err => {
                      if (err) res.status(500).send(err)
                      else res.status(201).json(reservation);
                    });

                  } else { // restaurent is booked
                    res.status(400).json({
                      errors: [{
                        "location": "body",
                        "param": "seats",
                        "value": _seats,
                        "msg": `Can only accomadate ${isNewReservationOnSameDateAndTime ? (seatCount - (currentReservationCount - seats)) : (seatCount - currentReservationCount)} people on this slot`
                      }]
                    });
                  }
                }
              });
            }
          });
        }
      });
    });

  ReservationRouter.route('/')
    .post([
      check('restaurantId').isMongoId().withMessage("This restaurant id doesn't seem to be valid"),
      check('seats').isInt().withMessage("Invalid number of seats"),
      check('date').isISO8601().withMessage("Invalid reservation date"),
      check('message').optional().isString().withMessage("Invalid message"),
      check('date').exists().custom((value, { req }) => {
        // check if the date is future date
        const { date: reqDate, slot: reqSlot } = req.body;
        const date = new Date(reqDate);
        const slot = new Date(reqSlot);
        const reservation = new Date(date.getFullYear(), date.getMonth(), date.getDate(), slot.getHours(), slot.getMinutes(), slot.getSeconds());
        return new Date(reservation) > new Date()
      }).withMessage("Reservations can be only added to future events"),
      check('slot').isISO8601().withMessage("Invalid reservation slot"),
    ],
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
        }

        const { restaurantId } = req.body;
        Restaurant.findById(restaurantId, (err, result) => {
          if (err || _.isEmpty(result)) {
            res.status(422).json({
              errors: [{
                "location": "body",
                "param": "restaurantId",
                "value": restaurantId,
                "msg": "Invalid restaurant id"
              }]
            });
          } else {
            const { seats: seatCount, serviceTimes } = result;
            const { date, restaurantId, slot, seats } = req.body;

            const _date = moment(date).format('YYYY-MM-DD') + 'T00:00:00.000Z'; // we just need the date
            const _slot = moment(slot).set({ year: 2001, month: 0, date: 1 }); // we need just the time

            if (!serviceTimes.some(e => e.toISOString() === _slot.toISOString())) {
              res.status(422).json({
                errors: [{
                  "location": "body",
                  "param": "slot",
                  "value": slot,
                  "msg": `Restaurent doesn't offer a ${_slot.format("h:mm a")} service`
                }]
              });
              return;
            }

            Reservation.aggregate([
              {
                $match: {
                  $and: [
                    { restaurantId: ObjectId(restaurantId) },
                    { date: new Date(_date) },
                    { slot: _slot.toDate() },
                  ]
                }
              },
              {
                $group: { _id: null, total: { $sum: "$seats" } }
              }
            ], function (err, result) {
              if (err) {
                res.status(422).json({
                  errors: [{
                    "location": "body",
                    "param": "restaurantId",
                    "value": restaurantId,
                    "msg": "Invalid restaurant id"
                  }]
                });
              } else {
                const currentReservationCount = result[0] ? result[0].total : 0;
                if ((_.isEmpty(result) && seats <= seatCount) || (currentReservationCount + seats) <= seatCount) { // looks like there are no bookings for this date and slot

                  const _reservation = Object.assign(req.body, { userId: req.user._id })
                  const reservation = new Reservation(_reservation);
                  reservation.save(err => {
                    if (err) res.status(500).send(err)
                    else res.status(201).json(reservation);
                  });

                } else { // restaurent is fully booked
                  res.status(400).json({
                    errors: [{
                      "location": "body",
                      "param": "seats",
                      "value": seats,
                      "msg": `Can only accomadate ${seatCount - currentReservationCount} more people on this slot`
                    }]
                  });
                }
              }
            });
          }
        })
      });

  return ReservationRouter;
}

module.exports = routerConstructor;