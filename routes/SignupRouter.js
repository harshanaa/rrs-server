const express = require('express');
const _ = require('lodash');
const { check, validationResult } = require('express-validator/check');

const routerConstructor = (User) => {
  const SignupRouter = express.Router();

  SignupRouter.route('/')
    .post([
      check('username').isEmail().withMessage('Email looks invalid'),
      check('password').isLength({ min: 5 }).withMessage('Password should be atleast 5 char long'),
      check('firstName').isString().withMessage('FirstName is a required field'),
      check('lastName').isString().withMessage('LastName is a required field'),
    ], (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const { username } = req.body;

      User.find({ username }, (err, user) => {
        if (err) {
          return res.status(500).send(err)
        } else if (!_.isEmpty(user)) {
          return res.status(400).json({
            errors: [{
              "location": "body",
              "param": "Email",
              "value": username,
              "msg": "Email is already in use"
            }]
          });
        } else {
          const user = new User(req.body);
          user.save(err => {
            const _user = user.toObject();
            delete _user.password;
            if (err) {
              return res.status(500).json({
                errors: [{
                  "msg": "Internal server error"
                }]
              });
            }
            else res.status(201).json(_user)
          });
        }
      })
    });

  return SignupRouter;
}

module.exports = routerConstructor;