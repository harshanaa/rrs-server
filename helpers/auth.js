const passport = require('passport');
const JwtBearerStrategy = require('passport-http-jwt-bearer');

const authConstructor = (User) => {
  passport.use(new JwtBearerStrategy(
    process.env.JWT_SECRET,
    (data, done) => {
      User.findById(data.id, (err, user) => {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user, data);
      });
    }
  ));

  return {
    initialize: () => {
      return passport.initialize();
    },
    authenticate: () => {
      return passport.authenticate("jwt-bearer", { session: false });
    }
  };
}

module.exports = authConstructor;