var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RestaurantModel = new Schema({
  name: { type: String, required: true, default: '' },
  description: { type: String, default: null },
  type: [String],
  logoUrl: { type: String, default: "http://via.placeholder.com/300/09f/fff.png" },
  images: [{ title: String, url: String }],
  rating: { type: Number, default: 0 },
  coordinates: { type: [Number], index: '2d', default: [null, null], required: true },
  serviceTimes: [{ type: Date }],
  seats: { type: Number, default: 0 }
});

module.exports = mongoose.model('Restaurant', RestaurantModel);