var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReservationModel = new Schema({
  restaurantId: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  seats: { type: Number, required: true },
  message: { type: String, default: '' },
  date: { type: Date, required: true }, // date only : new Date("2018-07-22") => "2018-07-22T00:00:00.000Z",
  slot: { type: Date, required: true }, // time only : var a = new Date(); a.setYear(2000); a.setMonth(0); a.setDate(1); => "2000-01-01T15:00:00.704Z"
  created: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Reservation', ReservationModel);