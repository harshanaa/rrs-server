var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var userModel = new Schema({
  firstName: { type: String, default: '' },
  lastName: { type: String, default: '' },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  created: { type: Date, default: Date.now }
});

userModel.pre("save", function (next) {
  let user = this;
  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, function (err, hash) {
      user.password = hash;
      next();
    });
  });
});

userModel.methods.comparePassword = function (_password, cb) {
  let user = this;
  bcrypt.compare(_password, user.password, function (err, success) {
    if (err) return cb(err);
    cb(null, success);
  });
};

module.exports = mongoose.model('User', userModel);