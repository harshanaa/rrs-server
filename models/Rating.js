var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RatingModel = new Schema({
  restaurantId: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  rating: { type: Number, required: true },
  created: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Rating', RatingModel);