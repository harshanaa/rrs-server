# rrs-server
Restaurent reservation system consist 2 projects. This is the node.js based server implementation.

## Run Locally

To clone and run this project, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://harshanaa@bitbucket.org/harshanaa/rrs-server.git
# Go into the repository
cd rrs-server
# Install dependencies
npm install or yarn add
# Run the app
npm start
```

## Custom settings

.env file is attached to the repo, so you can run it without modifications.